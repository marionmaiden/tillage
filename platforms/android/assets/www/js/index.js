/**
 * Index.html JS
 * @type type
 */
//==============================================================================
//                 APP OBJECT
//==============================================================================
var app = {
    // Application Constructor. Called in the end of this file
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    onDeviceReady: function () {
        /* When clicking on a external link, shows the loader screen
        $('a[href][rel=external]').click(function (e) {
            utils.showLoading();
        });*/
        
        // Setting the theme based on config
        Config.setTheme();
        
        // Before showing the Tillage List Page (listTillages.html) 
        // fill the list based on the tillage file names
        $(document).on('pagebeforeshow', "#listTillages", function (event, data) {
            Tillage.fillTillageList();
        });

        // Before showing the Sample List Page (listSamples.html) 
        // open the tillage file to show the samples inside
        $(document).on('pagebeforeshow', "#listSamples", function (event, data) {
            var parameter = $(this).data("url").split("?")[1];
            parameter = parameter.replace("file=", "");
            Tillage.openTillage(parameter);
        });

        // Before showing the Edit Agenda Page (editAgenda.html) 
        // open the tillage file to show the samples inside
        $(document).on('pagebeforeshow', "#editAgenda", function (event, data) {
            var parameter = $(this).data("url").split("?")[1];
            parameter = parameter.replace("agenda=", "");
            Event.openAgenda(parseInt(parameter));
        });
        
        // Before showing the Edit Check Page (editCheck.html) 
        // open the tillage file to show the samples inside
        $(document).on('pagebeforeshow', "#editCheck", function (event, data) {
            var parameter = $(this).data("url").split("?")[1];
            parameter = parameter.replace("check=", "");
            Event.openCheck(parseInt(parameter));
        });
        


        // Before showing the Events List Page (listEvents.html) 
        // open the event file to show the events inside
        $(document).on('pagebeforeshow', "#listEvents", function (event, data) {
            Event.openEvent();
        });
    }
};

/**
 * Initializes the App
 */
app.initialize();
