var Config = {
    
    /**
     * Change the default theme
     * @returns {undefined}
     */
    changeTheme : function () {
               
        if(window.localStorage.getItem("theme") === constants.THEME_DARK){
            $("#jQMnDTheme").attr({'href': 'js/libs/nDroid/css/jquerymobile.nativedroid.light.css'});
            window.localStorage.setItem("theme", constants.THEME_LIGHT);
        }
        else {
            $("#jQMnDTheme").attr({'href': 'js/libs/nDroid/css/jquerymobile.nativedroid.dark.css'});
            window.localStorage.setItem("theme", constants.THEME_DARK);
        }
        
    },
    /**
     * Set the default theme
     * @returns {undefined}
     */
    setTheme : function () {
        if (window.localStorage.getItem("theme") === null ||
                window.localStorage.getItem("theme") !== constants.THEME_DARK) {
            // Set default Theme
            window.localStorage.setItem("theme", constants.THEME_DARK);
        }
        
        if(window.localStorage.getItem("theme") === constants.THEME_LIGHT){
            $("#jQMnDTheme").attr({'href': 'js/libs/nDroid/css/jquerymobile.nativedroid.light.css'});
        }
    }
};
