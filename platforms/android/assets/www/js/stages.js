var Stages = {
    V0: "",
    VE: "<br>\n\
        <h3 class='mark'>Estádio de Emergencia VE</h3> \n\
        <p>- Este estádio vai semeadura à emergência</p> \n\
        <p>- A emergência deve ocorrer de 4 a 5 dias após o plantio</p>\n\
        <p><span class='mark'>Monitorar:</span></p> \n\
        <p>Pragas de solo e doença de sementes</p>",
    V1: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V1</h3> \n\
        <p>- 1 Semana</p>\n\
        <p>- Neste estádio a planta tem 1 folha totalmente expandida</p> \n\
        <p>- Deve ser feito: Adubação de cobertura e controle de ervas daninhas</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de ervas daninhas</p>\n\
        <p>Morte de plantas</p>",
    V2: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V2</h3> \n\
        <p>- Neste estádio a planta tem 2 folhas totalmente expandidas</p> \n\
        <p>- Deve ser feito: Adubação de cobertura e controle de ervas daninhas</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de ervas daninhas</p>\n\
        <p>Morte de plantas</p>",
    V3: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V3</h3> \n\
        <p>- 2 Semanas</p>\n\
        <p>- Neste estádio a planta tem 3 folhas totalmente expandidas</p> \n\
        <p>- Deve ser feito: Limite máximo para adubação de cobertura e controle de ervas daninhas</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Stand: Número de plantas por metro linear</p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de ervas daninhas</p>",
    V4: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V4</h3> \n\
        <p>- Neste estádio a planta tem 4 folhas totalmente expandidas</p> \n\
        <p>- Deve ser feito: Limite máximo para controle de ervas daninhas</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de ervas daninhas</p>",
    V5: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V5</h3> \n\
        <p>- Neste estádio a planta tem 5 folhas totalmente expandidas</p> \n\
        <p>- Deve ser feito: Ponto de maior competição entre milho e ervas daninhas</p>\n\
        <p>- Necessidade de água: 4-5 mm/dia</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de ervas daninhas</p>",
    V6: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V6</h3> \n\
        <p>- Neste estádio a planta tem 6 folhas totalmente expandidas</p> \n\
        <p>- Necessidade de água: 4-5 mm/dia</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Stand: Número de plantas por metro linear</p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V7: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V7</h3> \n\
        <p>- 4 Semanas</p>\n\
        <p>- Neste estádio a planta tem 7 folhas totalmente expandidas</p> \n\
        <p>- Necessidade de água: 4-5 mm/dia</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Stand: Número de plantas por metro linear</p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V8: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V8</h3> \n\
        <p>- Neste estádio a planta tem 8 folhas totalmente expandidas</p> \n\
        <p>- Limite para entrada de implementos na área</p>\n\
        <p>- Maiores perdas decorrentes de chuvas de granizo e ataque de lagartas</p>\n\
        <p>- Maior exigência de água: 5 – 7 mm</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V9: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V9</h3> \n\
        <p>- Neste estádio a planta tem 9 folhas totalmente expandidas</p> \n\
        <p>- Maiores perdas decorrentes de chuvas de granizo e ataque de lagartas</p>\n\
        <p>- Maior exigência de água: 5 – 7mm</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V10: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V10</h3> \n\
        <p>- 6 Semanas</p>\n\
        <p>- Neste estádio a planta tem 10 folhas totalmente expandidas</p> \n\
        <p>- Maiores perdas decorrentes de chuvas de granizo e ataque de lagartas</p>\n\
        <p>- Maior exigência de água: 5 – 7mm</p>\n\
        <p>- Realizar aplicação de fungicida</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V11: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V11</h3> \n\
        <p>- Neste estádio a planta tem 11 folhas totalmente expandidas</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V12: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V12</h3> \n\
        <p>- Neste estádio a planta tem 12 folhas totalmente expandidas</p> \n\
        <p>- Maior necessidade de água: 7-8 mm/ dia</p>\n\
        <p>- A planta atinge cerca de 85% a 90% da área foliar</p>\n\
        <p>- Início do desenvolvimento das raízes adventícias (“esporões”)</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V13: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V13</h3> \n\
        <p>- Neste estádio a planta tem 13 folhas totalmente expandidas</p> \n\
        <p>- Maior exigência de água</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V14: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V14</h3> \n\
        <p>- Neste estádio a planta tem 14 folhas totalmente expandidas</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V15: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V15</h3> \n\
        <p>- Neste estádio a planta tem 15 folhas totalmente expandidas</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V16: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V16</h3> \n\
        <p>- Neste estádio a planta tem 16 folhas totalmente expandidas</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V17: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V17</h3> \n\
        <p>- Neste estádio a planta tem 17 folhas totalmente expandidas</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    V18: "<br>\n\
        <h3 class='mark'>Estádio Vegetativo V18</h3> \n\
        <p>- Neste estádio a planta tem 18 folhas totalmente expandidas</p> \n\
        <p>- Maiores perdas decorrentes de chuvas de granizo e ataque de lagartas</p>\n\\n\
        <p>- Encontra-se a uma semana do florescimento</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    VT: "<br>\n\
        <h3 class='mark'>Estádio VT  Pendoamento</h3> \n\
        <p>- 8 Semanas</p>\n\
        <p>- Alta exigência de água: 7-8 mm/ dia</p> \n\
        <p>- Realizar aplicação de fungicida</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças</p>",
    R1: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R1 (9 a 10 sem.)</h3> \n\
        <p>- Embonecamento</p> \n\
        <p>- Esse estádio é iniciado quando os estilos-estigmas estão visíveis, para fora das espigas</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>\n\
        <p>Presença de fungos em áreas de alta umidade</p>",
    R2: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R2-Aquoso</h3> \n\
        <p>- 12 Dias após a polinização</p>\n\
        <p>- Grãos brancos com conteúdo fluido e claro internamente </p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>\n\
        <p>Presença de fungos em áreas de alta umidade</p>",
    R3: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R3-Leitoso</h3> \n\\n\
        <p>- 24 Dias após a polinização</p>\n\
        <p>- Fluido de cor leitosa </p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>\n\
        <p>Presença de fungos em áreas de alta umidade</p>",
    R4: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R4-Pastoso</h3> \n\\n\
        <p>- 36 Dias após a polinização</p>\n\
        <p>- Interior do grão tem consistência pastosa</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>\n\
        <p>Presença de fungos em áreas de alta umidade</p>",
    R5: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R5-Dentes</h3> \n\
        <p>- 48 Dias após a polinização</p>\n\
        <p>- Formação de uma concavidade na parte superior do grão</p> \n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>",
    R6: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R6-Maturidade</h3> \n\
        <p>- Máximo de acumulo de peso seco dos grãos</p> \n\
        <p>- Folhas começam a secar</p>\n\
        <p>- Colher com 25% de umidade e armazenar com menos de 13%</p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p>Presença de pragas</p>\n\
        <p>Presença de indicativo de doenças nas espigas</p>\n\
        <p>Presença de fungos em áreas de alta umidade</p>",
    R7: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R7</h3> \n\
        <p>- </p> \n\
        <p>- </p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p></p>\n\
        <p></p>",
    R8: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R8</h3> \n\
        <p>- </p> \n\
        <p>- </p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p></p>\n\
        <p></p>",
    R9: "<br>\n\
        <h3 class='mark'>Estádio Reprodutivo R9</h3> \n\
        <p>- </p> \n\
        <p>- </p>\n\
        <p><span class='mark'>Monitorar:</span></p>\n\
        <p></p>\n\
        <p></p>",
    
    onChangeState: function(fenState){
        $("#stageVal").html(Stages[""+fenState]);
    }
};