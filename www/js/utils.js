//==============================================================================
//                 UTILS FUNCTIONS
//==============================================================================

/**
 * utils functions
 * @type type
 */
var utils = {
    /**
     * Get Today date
     * @returns {String}
     */
    getToday: function () {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return yyyy + "-" + mm + "-" + dd;
    },
    dateDiff: function (date2) {
        var start = new Date(date2);
        var end = new Date();
        var diff = new Date(end - start);
        var days = diff / 1000 / 60 / 60 / 24;

        return Math.floor(days);

    },
    /**
     * Calculates the remaining time to harvest the corn
     * If the harvest time has expired, returns 0
     * @param {type} diff
     * @returns {undefined}
     */
    getRemainingTime: function (diff) {
        var remaining = constants.TILLAGE_DAYS - diff;
        return remaining > 0 ? remaining : 0;
    },
    /**
     * Show loading message
     * @returns {undefined}
     */
    showLoading: function () {
        $.mobile.loading("show", {
            text: "Carregando",
            textVisible: true,
            textonly: false,
            theme: "b",
            html: ""
        });
    },
    /**
     * From a tillage file name, returns a string with the name to be shown
     * in the list
     * @param {type} fileName
     * @returns {String}
     */
    getTillageName: function (fileName) {
        var index = fileName.indexOf("_");
        var date = fileName.substring(0, index);
        var name = fileName.substring(index + 1);
        return "Data: " + date + " - " + name;
    },
    /**
     * From a tillage file name, returns a string with the name to be shown
     * in the list
     * @param {type} fileName
     * @returns {String}
     */
    getTillageDate: function (fileName) {
        var index = fileName.indexOf("_");
        var date = new Date (fileName.substring(0, index));
        return date;
    },
    blockBackButton: function () {
        // add listener to prevent back button
        document.addEventListener("backbutton", utils.onBackButton);
    },
    unblockBackButton: function () {
        // leaving the page, enables again the back button
        document.removeEventListener("backbutton", utils.onBackButton, false);
    },
    onBackButton: function (e) {
        $("#listSamplesAnchor").click();
        document.removeEventListener("backbutton", utils.onBackButton, false);
    }
};

