//==============================================================================
//                 SAMPLE TABLE OBJECT
//==============================================================================

/**
 * Object to take care of the table showed after the sample insertion
 * @type type
 */
var SampleTable = {
    sampleSummary: function (sampleIndex) {
        SampleTable.fillPlantsRow("#plantParam", sampleIndex);
        SampleTable.fillHerbsRow("#weedParam", sampleIndex);
        SampleTable.fillPlaguesRow("#plagueParam", sampleIndex);
        SampleTable.fillDiseasesRow("#diseaseParam", sampleIndex);
        SampleTable.fillProductionRow("#productionParam", sampleIndex);
    },
    /**
     * 
     * @param {type} selector
     * @param {type} index
     * @returns {undefined}
     */
    fillPlantsRow: function (selector, index) {
        var txt = "";
        var i;
        var average = 0;

        for (i = 0; i < constants.AREA_COUNT; i++) {
            average += Number(Tillage.tillage.samples.arrSample[index].areas.array[i].plants);
            txt += "<td class=\"ui-table-priority-2\">" +
                    Tillage.tillage.samples.arrSample[index].areas.array[i].plants +
                    "</td>";
        }

        average /= constants.AREA_COUNT;

        // Include average value
        if (average > 10) {
            txt += "<td class=\"above\"><i class=\"fa fa-thumbs-up\"></i> " + average + "</td>";
        }
        else if (average < 10) {
            txt += "<td class=\"below\"><i class=\"fa fa-thumbs-down\"></i> " + average + "</td>";
        }
        else {
            txt += "<td style=\"font-weight: bold\">" + average + "</td>";
        }

        // Include goal value
        txt += "<td>" + 10 + "</td>";

        // Remove previous data and append the value to the table
        $(selector + ' td').remove();
        $(selector).append(txt);
    },
    /**
     * 
     * @param {type} selector
     * @param {type} index
     * @returns {undefined}
     */
    fillHerbsRow: function (selector, index) {
        var txt = "";
        var i;
        var average = false;
        var checked;

        for (i = 0; i < constants.AREA_COUNT; i++) {
            checked = Tillage.tillage.samples.arrSample[index].areas.array[i].herbs;
            
            if(checked){
                average = true;
                txt += "<td class=\"ui-table-priority-2\">com</td>";
            }
            else {
                txt += "<td class=\"ui-table-priority-2\">sem</td>";
            }
        }

        // Include average value
        if (average) {
            txt += "<td class=\"below\"><i class=\"fa fa-thumbs-down\"></i>com</td>";
        }
        else {
            txt += "<td class=\"above\"><i class=\"fa fa-thumbs-up\"></i>sem</td>";
        }

        // Include goal value
        txt += "<td class=\"ui-table-priority-1\">sem</td>";

        // Remove previous data and append the value to the table
        $(selector + ' td').remove();
        $(selector).append(txt);
    },
    /**
     * 
     * @param {type} selector
     * @param {type} index
     * @returns {undefined}
     */
    fillPlaguesRow: function (selector, index) {
        var txt = "";
        var i;
        var average = 0;

        for (i = 0; i < constants.AREA_COUNT; i++) {
            average += Number(Tillage.tillage.samples.arrSample[index].areas.array[i].plague);
            txt += "<td class=\"ui-table-priority-2\">" +
                    Tillage.tillage.samples.arrSample[index].areas.array[i].plague +
                    "</td>";
        }

        average /= constants.AREA_COUNT;

        // Include average value
        if (average > 10) {
            txt += "<td class=\"above\"><i class=\"fa fa-thumbs-up\"></i> " + average + "</td>";
        }
        else if (average < 10) {
            txt += "<td class=\"below\"><i class=\"fa fa-thumbs-down\"></i> " + average + "</td>";
        }
        else {
            txt += "<td style=\"font-weight: bold\">" + average + "</td>";
        }

        // Include goal value
        txt += "<td class=\"ui-table-priority-1\">" + 10 + "</td>";

        // Remove previous data and append the value to the table
        $(selector + ' td').remove();
        $(selector).append(txt);
    },
    /**
     * 
     * @param {type} selector
     * @param {type} index
     * @returns {undefined}
     */
    fillDiseasesRow: function (selector, index) {
        var txt = "";
        var i;
        var average = false;
        var checked;

        for (i = 0; i < constants.AREA_COUNT; i++) {
            checked = Tillage.tillage.samples.arrSample[index].areas.array[i].disease;
            
            if(checked){
                average = true;
                txt += "<td class=\"ui-table-priority-2\">com</td>";
            }
            else {
                txt += "<td class=\"ui-table-priority-2\">sem</td>";
            }
        }

        // Include average value
        if (average) {
            txt += "<td class=\"below\"><i class=\"fa fa-thumbs-down\"></i>com</td>";
        }
        else {
            txt += "<td class=\"above\"><i class=\"fa fa-thumbs-up\"></i>sem</td>";
        }

        // Include goal value
        txt += "<td class=\"ui-table-priority-1\">sem</td>";

        // Remove previous data and append the value to the table
        $(selector + ' td').remove();
        $(selector).append(txt);
    },
    /**
     * 
     * @param {type} selector
     * @param {type} index
     * @returns {undefined}
     */
    fillProductionRow: function (selector, index) {
        var txt = "";
        var i;
        var average = 0;

        for (i = 0; i < constants.AREA_COUNT; i++) {
            average += Number(Tillage.tillage.samples.arrSample[index].areas.array[i].production);
            txt += "<td class=\"ui-table-priority-2\">" +
                    Tillage.tillage.samples.arrSample[index].areas.array[i].production +
                    "</td>";
        }

        average /= constants.AREA_COUNT;

        // Include average value
        if (average > 10) {
            txt += "<td class=\"above\"><i class=\"fa fa-thumbs-up\"></i> " + average + "</td>";
        }
        else if (average < 10) {
            txt += "<td class=\"below\"><i class=\"fa fa-thumbs-down\"></i> " + average + "</td>";
        }
        else {
            txt += "<td style=\"font-weight: bold\">" + average + "</td>";
        }

        // Include goal value
        txt += "<td class=\"ui-table-priority-1\">" + 10 + "</td>";

        // Remove previous data and append the value to the table
        $(selector + ' td').remove();
        $(selector).append(txt);
    }
};

