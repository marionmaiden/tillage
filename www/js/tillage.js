//==============================================================================
//                 TILLAGE OBJECT
//==============================================================================

var Tillage = {
    openedTillage: "",
    /**
     * Temporary Tillage Sample object
     * @type type
     */
    tempSample: {
        date: "",
        responsible: "",
        fenStatus: "",
        areas: {
            array: [
                {plants: 0, herbs: 0, plague: 0, disease: 0, rain: 0, temp: 0, production: 0},
                {plants: 0, herbs: 0, plague: 0, disease: 0, rain: 0, temp: 0, production: 0},
                {plants: 0, herbs: 0, plague: 0, disease: 0, rain: 0, temp: 0, production: 0},
                {plants: 0, herbs: 0, plague: 0, disease: 0, rain: 0, temp: 0, production: 0},
                {plants: 0, herbs: 0, plague: 0, disease: 0, rain: 0, temp: 0, production: 0}
            ]}
    },
    /**
     * Initialize the temp Sample object with empty data
     * @returns {undefined}
     */
    initTempSample: function () {
        this.tempSample.date = "";
        this.tempSample.responsible = "";
        this.tempSample.fenStatus = "";
        var i;
        for (i = 0; i < 5; i++) {
            this.tempSample.areas.array[i] = {plants: 0,
                herbs: 0,
                plague: 0,
                disease: 0,
                rain: 0,
                temp: 0,
                production: 0};
        }
    },
    getNumberSamples: function () {
        return Tillage.tillage.samples.arrSample.length;
    },
    /**
     * Tillage Object
     * @type type
     */
    tillage: {
        nameTillage: "",
        dateInit: "",
        type: "CORN",
        spacing: 0,
        samples: {arrSample: []}
    },
    /**
     * Initialize the Tillage object with empty data
     * @returns {undefined}
     */
    initTillage: function () {
        this.tillage.nameTillage = "";
        this.tillage.dateInit = "";
        this.tillage.type = "CORN";
        this.tillage.spacing = 0;
        while (this.getNumberSamples()) {
            this.tillage.samples.arrSample.pop();
        }
    },
    /**
     * Saves a tillage
     * @returns {undefined}
     */
    saveNewTillage: function () {
        this.initTillage();
        this.tillage.nameTillage = $("#nameTillageTxt").val();
        this.tillage.dateInit = $("#dateInitTxt").val();
        this.tillage.spacing = $("#spacingTxt").val();

        // write the tillage object on dist
        this.writeTillage();
    },
    /**
     * Writes a Tillage object on disk
     * @returns {undefined}
     */
    writeTillage: function () {
        var tillageJSON = JSON.stringify(Tillage.tillage);
        var filename = Tillage.tillage.dateInit + "_" + Tillage.tillage.nameTillage;

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            fileSystem.root.getDirectory("tillage", {create: true}, function (dirEntry) {
                dirEntry.getFile(filename, {create: true}, function (fileEntry) {
                    fileEntry.createWriter(function (writer) {
                        writer.write(tillageJSON);
                    });
                });
            });
        });
    },
    /**
     * Open a tillage, based on the filename passed as parameter
     * @param {type} fileName
     * @returns {undefined}
     */
    openTillage: function (fileName) {
        this.initTillage();
        this.openedTillage = fileName;

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
        function (fileSystem) {
            fileSystem.root.getDirectory("tillage", {create: false},
            function (dirEntry) {
                dirEntry.getFile(fileName, {create: false, exclusive: true},
                function (fileEntry) {
                    fileEntry.file(
                    function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (evt) {
                            Tillage.tillage = JSON.parse(evt.target.result);
                            Tillage.fillSampleList();
                        };
                        reader.readAsText(file);
                    }, null);
                });
            });
        });
    },
    /**
     * Searches for the tillage files to put the names in a list
     * @returns {undefined}
     */
    fillTillageList: function () {
        // Clean the old list before show
        $('#tillageList li').remove();
        // Open the event file
        Event.openEvent();
        
        // Creates a folder called "tillage", if it still doesn't exists
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
        function (fileSystem) {
            fileSystem.root.getDirectory("tillage", {create: true},
            function (dirEntry) {
                var directoryReader = dirEntry.createReader();
                directoryReader.readEntries(
                function (entries) {
                    var i;
                    var txt = "";
                    var counter = 0;
                    var tillageDate;
                    
                    for (i = 0; i < entries.length; i++) {
                        // If the list has more than one element and it is the FIRST one
                        if (i === 0) {                            
                            tillageDate = utils.getTillageDate(entries[i].name);
                            // Searches for checklist close to the date
                            counter = Event.countCheckList(tillageDate);
                            
                            txt += "<li class='ui-first-child'>" +
                                    "<a href='listSamples.html?file=" + entries[i].name + "' data-transition='slide' " +
                                    "data-customid='" + entries[i].name + "' class='ui-btn'>" +
                                    utils.getTillageName(entries[i].name) +
                                        "<a href='#nextEvents' class='ui-li-count ui-btn-up-b ui-btn-corner-all'" + 
                                        "data-transition='pop' data-rel='popup' data-position-to='window'>" + counter + "</a>" +
                                    "</a></li>";
                        }
                        // If the list has more than one element and it is the LAST one
                        else if (i === entries.length - 1) {
                            tillageDate = utils.getTillageDate(entries[i].name);
                            // Searches for checklist close to the date
                            counter = Event.countCheckList(tillageDate);
                            
                            txt += "<li class='ui-last-child'>" +
                                    "<a href='listSamples.html?file=" + entries[i].name + "' data-transition='slide' " +
                                    "data-customid='" + entries[i].name + "' class='ui-btn'>" +
                                    utils.getTillageName(entries[i].name) +
                                        "<a href='#nextEvents' class='ui-li-count ui-btn-up-b ui-btn-corner-all' " + 
                                        "data-transition='pop' data-rel='popup' data-position-to='window'>" + counter + "</a>" +
                                    "</a></li>";
                        }
                        // For the other cases
                        else {
                            tillageDate = utils.getTillageDate(entries[i].name);
                            // Searches for checklist close to the date
                            counter = Event.countCheckList(tillageDate);
                            
                            txt += "<li>" +
                                    "<a href='listSamples.html?file=" + entries[i].name + "' data-transition='slide' " +
                                    "data-customid='" + entries[i].name + "' class='ui-btn'>" +
                                    utils.getTillageName(entries[i].name) +
                                        "<a href='#nextEvents' class='ui-li-count ui-btn-up-b ui-btn-corner-all'" + 
                                        "data-transition='pop' data-rel='popup' data-position-to='window'>" + counter + "</a>" +
                                    "</a></li>";
                        }
                    }
                    // Append the list on the <ol>
                    $("#tillageList").append(txt);
                });
            });
        }, null);
    },
    fillSampleList: function () {
        var i;
        var txt = "";

        // empty the old data before fill the list 
        $('#sampleList li').remove();

        if (this.getNumberSamples() > 0) {
            for (i = 0; i < this.getNumberSamples(); i++) {
                // If the list has only one element
                if (this.tillage.samples.arrSample.length === 1) {
                    txt += "<li class='ui-first-child ui-last-child row'>" +
                            "<a href='editSample3.html?sample=" + i +
                            "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-customid='" + i + "' >" +
                            "Amostra: " + this.tillage.samples.arrSample[i].date +
                            "</a></li>";
                }
                // If the list has more than one element and it is the FIRST one
                else if (i === 0) {
                    txt += "<li class='ui-first-child row'>" +
                            "<a href='editSample3.html?sample=" + i +
                            "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-customid='" + i + "' >" +
                            "Amostra: " + this.tillage.samples.arrSample[i].date +
                            "</a></li>";
                }
                // If the list has more than one element and it is the LAST one
                else if (i === this.getNumberSamples() - 1) {
                    txt += "<li class='ui-last-child row'>" +
                            "<a href='editSample3.html?sample=" + i +
                            "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-customid='" + i + "' >" +
                            "Amostra: " + this.tillage.samples.arrSample[i].date +
                            "</a></li>";
                }
                // For the other cases
                else {
                    txt += "<li class='row'>" +
                            "<a href='editSample3.html?sample=" + i + "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-customid='" + i + "' >" +
                            "Amostra: " + this.tillage.samples.arrSample[i].date +
                            "</a></li>";
                }
            }
            // Append the list on the <ul>
            $("#sampleList").append(txt);
        }
    },
    saveSample: function (step) {
        switch (step) {
            case 1 :
                this.saveSampleStage1();
                break;
            case 2 :
                this.saveSampleStage2();
                break;
            default :
                break;
        }
    },
    saveSampleStage1: function () {
        this.initTempSample();
        this.tempSample.date = $("#dateMeasureTxt").val();
        this.tempSample.responsible = $("#responsibleTxt").val();
        this.tempSample.fenStatus = $("#fenStateTxt").val();
    },
    saveSampleStage2: function () {
        var i;

        for (i = 1; i <= constants.AREA_COUNT; i++) {
            this.tempSample.areas.array[i - 1].plants = $("#c" + i).val();
            this.tempSample.areas.array[i - 1].herbs = $("#d" + i).prop('checked');
            this.tempSample.areas.array[i - 1].plague = $("#p" + i).val();
            this.tempSample.areas.array[i - 1].disease = $("#i" + i).prop('checked');
            this.tempSample.areas.array[i - 1].production = $("#pr" + i).val();
        }

        this.tillage.samples.arrSample.push(Tillage.tempSample);
        this.writeTillage();
    },
    /**
     * Deletes a specific sample
     * @param {type} ind
     * @returns {undefined}
     */
    deleteSample: function (ind) {
        this.tillage.samples.arrSample.splice(ind, 1);
        this.fillSampleList();
        this.writeTillage();
    },
    /**
     * Deletes a specific Tillage
     * @param {type} fileName
     * @returns {undefined}
     */
    deleteTillage: function (fileName) {
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
        function (fileSystem) {
            fileSystem.root.getDirectory("tillage", {create: false},
            function (dirEntry) {
                dirEntry.getFile(fileName, {create: false, exclusive: true},
                function (fileEntry) {
                    fileEntry.remove();
                });
            });
        });
    }
};


