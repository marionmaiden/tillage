//==============================================================================
//                 SYSTEM CONSTANTS
//==============================================================================

/**
 * System Constants
 * @type type
 */
var constants = {
    // Number of days of a complete tillage untill harvest
    TILLAGE_DAYS: 110,
    // Number of areas observed during a sample analysis
    AREA_COUNT: 5,
    // Theme color is light
    THEME_LIGHT: "1",
    // Theme color is dark
    THEME_DARK: "0",
    // Days before an event included on a search
    DAYS_BEFORE: 10
};


