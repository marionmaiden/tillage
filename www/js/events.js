/* global LocalFileSystem */

var Event = {
    eventsTxt: "",
    /**
     * Event Object
     * @type type
     */
    events: {
        // Stores events during a crop
        checkList: {checkListArray: []},
        // Stores events during the year
        agenda: {agendaArray: []}
    },
    tempAgenda: {
        date: "",
        text: ""
    },
    tempCheck: {
        day: "",
        text: ""
    },
    /**
     * Init the event object
     * @returns {undefined}
     */
    initEvt: function () {
        // Clean the checklist array
        while (this.events.checkList.checkListArray.length) {
            this.events.checkList.checkListArray.pop();
        }
        // Clean the agenda array
        while (this.events.agenda.agendaArray.length) {
            this.events.agenda.agendaArray.pop();
        }
    },
    initTempAgenda: function () {
        this.tempAgenda.date = "";
        this.tempAgenda.text = "";
    },
    initTempCheck: function () {
        this.tempCheck.day = "";
        this.tempCheck.text = "";
    },
    /**
     * Open an event, based on the filename that stores the events
     * @returns {undefined}
     */
    openEvent: function () {
        // Check if the default events weren't loaded
        if (window.localStorage.getItem("defaultEvents") === null) {
            window.localStorage.setItem("defaultEvents", "1");

            // Import the default JSON file
            $.getJSON("./json/events.json", function (data) {
                Event.events = data.events;
                Event.fillEventList();
            });
        }
        else {
            // Import the edited JSON file
            this.openEventFile();
        }
    },
    /**
     * Open a event file
     * @returns {undefined}
     */
    openEventFile: function () {
        this.initEvt();

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
                function (fileSystem) {
                    fileSystem.root.getDirectory("tillageEvt", {create: false},
                    function (dirEntry) {
                        dirEntry.getFile("events.json", {create: false, exclusive: true},
                        function (fileEntry) {
                            fileEntry.file(
                                    function (file) {
                                        var reader = new FileReader();
                                        reader.onloadend = function (evt) {
                                            // when the file is able to open, loads the event list
                                            Event.events = JSON.parse(evt.target.result);
                                            Event.fillEventList();
                                        };
                                        reader.readAsText(file);
                                    }, function () {
                                console.log("ERRO");
                            });
                        });
                    });
                });
    },
    /**
     * Writes the events object on disk
     * @returns {undefined}
     */
    writeEvents: function () {
        var evtJSON = JSON.stringify(Event.events);

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            fileSystem.root.getDirectory("tillageEvt", {create: true}, function (dirEntry) {
                dirEntry.getFile("events.json", {create: true, exclusive: false}, function (fileEntry) {
                    fileEntry.createWriter(function (writer) {
                        writer.write(evtJSON);
                    });
                });
            });
        });
    },
    /***************************************************************************
     Checklist Functions
     ***************************************************************************/


    /**
     * Searches on checklist items events close to the current date
     * @param {type} tillageDay
     * @returns {undefined}
     */
    countCheckList: function (tillageDay) {
        var elapsedTime = utils.dateDiff(tillageDay);
        var i, diff, counter = 0;

        // Searches the checklist array
        for (i = 0; i < this.events.checkList.checkListArray.length; i++) {
            diff = this.events.checkList.checkListArray[i].day - elapsedTime;
            if (diff > 0 && diff <= constants.DAYS_BEFORE) {
                counter++;
            }
        }
        return counter;
    },
    /**
     * @param {type} tillageDay
     * @returns {undefined}
     */
    searchCheckList: function (tillageDay) {
        var elapsedTime = utils.dateDiff(tillageDay);
        var i, j, diff;

        this.eventsTxt = "";

        // Searches the checklist array
        for (i = 0; i < this.events.checkList.checkListArray.length; i++) {
            diff = this.events.checkList.checkListArray[i].day - elapsedTime;

            // if a event is found between the searched period
            if (diff > 0 && diff <= constants.DAYS_BEFORE) {
                // adds the event to String
                this.eventsTxt += "Dia:" + this.events.checkList.checkListArray[i].day +
                        "\nAtividades:\n";
                this.eventsTxt += this.events.checkList.checkListArray[i].text;
            }
        }
    },
    /**
     * Open i-th element of checklist
     * Loads to the editCheck.html page
     * @param {type} i  if i = -1, means that a new check is being edited
     * @returns {undefined}
     */
    openCheck: function (i) {
        // writes the agenda ID to retrieve on save
        $("#idCheck").val(i);

        if (i !== -1) {
            var checkItem = this.events.checkList.checkListArray[i];

            $("#checkDate").val(checkItem.day);
            $("#checkTxt").html(checkItem.text);
        }
    },
    /**
     * Add element to Checklist
     * @returns {undefined}
     */
    addCheck: function () {
        this.initTempCheck();

        this.tempCheck.day = $("#checkDate").val();
        this.tempCheck.text = $("#checkTxt").html();
        this.events.checkList.checkListArray.push(this.tempCheck);

        // rewrite the events file
        this.writeEvents();
    },
    /**
     * Change the contents of the i-th element of check array
     * @param {type} i
     * @returns {undefined}
     */
    editCheck: function (i) {
        this.initTempCheck()();

        this.tempCheck.day = $("#checkDate").val();
        this.tempCheck.text = $("#checkTxt").html();

        this.events.checkList.checkListArray[i] = this.tempCheck;

        // rewrite the events file
        this.writeEvents();
    },
    /**
     * Remove some element of the check array
     * @param {type} i
     * @returns {undefined}
     */
    deleteCheck: function (i) {
        this.events.checkList.checkListArray.splice(i, 1);
        this.fillEventList();
        // rewrite the events file
        this.writeEvents();
    },
    /**
     * Fill the event list with the checklist and agenda data
     * @returns {undefined}
     */
    fillEventList: function () {
        var txt = "";

        // empty the old data before fill the list
        $("#eventList li").remove();

        // Get Agenda Events
        txt = this.fillAgenda();

        // Get CheckList Events
        txt += this.fillCheckList();

        // If some text is returned
        if (txt.length > 0) {
            $("#eventList").append(txt);
        }
        // Else if the list is empty
        else {
            alert("Não há eventos cadastrados");
        }
    },
    /**
     * Create a list with checklist items
     * @returns {String}
     */
    fillCheckList: function () {
        var i;
        var txt = "";

        // Checks if there is some element on list
        if (Event.events.checkList.checkListArray.length > 0) {
            // Create List Title

            txt += "<li class='ui-li ui-li-divider ui-first-child textCenter'" +
                    " data-role='list-divider'>Dia Após o Plantio</li>";

            for (i = 0; i < Event.events.checkList.checkListArray.length; i++) {
                // For the first elements
                if (i < Event.events.checkList.checkListArray.length - 1) {
                    txt += "<li class='row'>" +
                            "<a href='editCheck.html?check=" + i + "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-check='" + i + "' >" +
                            "Dia: " + Event.events.checkList.checkListArray[i].day +
                            "</a></li>";
                }
                // If it is the LAST one element of the list
                else {
                    txt += "<li class='ui-last-child row'>" +
                            "<a href='editCheck.html?check=" + i +
                            "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-check='" + i + "' >" +
                            "Dia: " + Event.events.checkList.checkListArray[i].day +
                            "</a></li>";
                }
            }
        }
        return txt;
    },
    /***************************************************************************
     Agenda Functions
     ***************************************************************************/

    /**
     * Create a list with agenda items
     * @returns {String}
     */
    fillAgenda: function () {
        var i;
        var txt = "";

        // Checks if there is some element on list
        if (Event.events.agenda.agendaArray.length > 0) {
            // Create List Title
            txt += "<li class='ui-li ui-li-divider ui-first-child textCenter'" +
                    "data-role='list-divider'>Eventos da Agenda</li>";

            for (i = 0; i < Event.events.agenda.agendaArray.length; i++) {
                // For the first elements
                if (i < Event.events.agenda.agendaArray.length - 1) {
                    txt += "<li class='row'>" +
                            "<a href='editAgenda.html?agenda=" + i + "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-agenda='" + i + "' >" +
                            "Data: " + Event.events.agenda.agendaArray[i].date +
                            "</a></li>";
                }
                // If it is the LAST one element of the list
                else {
                    txt += "<li class='ui-last-child row'>" +
                            "<a href='editAgenda.html?agenda=" + i +
                            "' data-transition='slide' class='ui-btn ui-btn-icon-right ui-icon-carat-r'" +
                            " data-agenda='" + i + "' >" +
                            "Data: " + Event.events.agenda.agendaArray[i].date +
                            "</a></li>";
                }
            }
        }
        return txt;
    },
    /**
     * Open i-th element of agenda
     * Loads to the editAgenda.html page
     * @param {type} i  if i = -1, means that a new agenda is being edited
     * @returns {undefined}
     */
    openAgenda: function (i) {
        // writes the agenda ID to retrieve on save
        $("#idAgenda").val(i);

        if (i !== -1) {
            var agendaItem = this.events.agenda.agendaArray[i];

            $("#dayAgenda").val(agendaItem.date.split('/')[0]);
            $("#monthAgenda").val(agendaItem.date.split('/')[1]);
            $("#agendaTxt").val(agendaItem.text);
        }
    },
    /**
     * Add element to Agenda
     * @returns {undefined}
     */
    addAgenda: function () {
        this.initTempAgenda();

        this.tempAgenda.date = $("#dayAgenda").val() + "/" + $("#monthAgenda").val();
        this.tempAgenda.text = $("#agendaTxt").val();
        this.events.agenda.agendaArray.push(this.tempAgenda);

        // rewrite the events file
        this.writeEvents();
    },
    /**
     * Change the contents of the i-th element of agenda array
     * @param {type} i
     * @returns {undefined}
     */
    editAgenda: function (i) {
        this.initTempAgenda();

        this.tempAgenda.date = $("#dayAgenda").val() + "/" + $("#monthAgenda").val();
        this.tempAgenda.text = $("#agendaTxt").val();

        this.events.agenda.agendaArray[i] = this.tempAgenda;

        // rewrite the events file
        this.writeEvents();
    },
    /**
     * Remove some element of the agenda array
     * @param {type} i
     * @returns {undefined}
     */
    deleteAgenda: function (i) {
        this.events.agenda.agendaArray.splice(i, 1);
        this.fillEventList();
        // rewrite the events file
        this.writeEvents();
    }
};

